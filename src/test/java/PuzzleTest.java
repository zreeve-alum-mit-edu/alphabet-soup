import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class PuzzleTest {

    @Test
    public void testSample1() throws IOException {
        //simple test
        Puzzle puzzle = new Puzzle("src\\test\\files\\sample1");
        puzzle.solve();

        Assert.assertEquals(3, puzzle.getRows());
        Assert.assertEquals(3, puzzle.getColumns());
        Assert.assertEquals(2, puzzle.getWords().get('A').size());
        Assert.assertArrayEquals(new int[] {0,0,0,2}, puzzle.getWords().get('A').get(0).getWord_coordinates().get(0));
    }

    @Test
    public void testSample2() throws IOException {
        //larger test
        Puzzle puzzle = new Puzzle("src\\test\\files\\sample2");
        puzzle.solve();

        Assert.assertArrayEquals(new int[] {0,0,4,4}, puzzle.getWords().get('H').get(0).getWord_coordinates().get(0));
    }

    @Test
    public void testMultiwords() throws IOException {
        Puzzle puzzle = new Puzzle("src\\test\\files\\multiwords");
        puzzle.solve();

        Assert.assertEquals(2, puzzle.getWords().get('G').get(0).getWord_coordinates().size());
    }

    @Test
    public void testSpaces() throws IOException {
        Puzzle puzzle = new Puzzle("src\\test\\files\\spaces");
        puzzle.solve();

        Assert.assertEquals(2, puzzle.getWords().get('G').get(0).getWord_coordinates().size());
    }

    @Test
    public void testLarge() throws IOException {
        Puzzle puzzle = new Puzzle("src\\test\\files\\large");
        puzzle.solve();

        Assert.assertEquals(3, puzzle.getWords().get('B').get(0).getWord_coordinates().size());
    }
}
