import java.util.ArrayList;
import java.util.List;

public class Word implements Comparable<Word> {
    private String word;
    //list of start and end coordinates
    private List<int[]> word_coordinates;

    public Word (String word) {
        this.word = word;
        word_coordinates = new ArrayList<>();
    }

    public List<int[]> getWord_coordinates() {
        return word_coordinates;
    }

    public String getWord() {
        return word;
    }

    //compare using the word string
    @Override
    public int compareTo(Word o) {
        return word.compareTo(o.getWord());
    }

    //equality based on the word string
    @Override
    public boolean equals(Object obj) {
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        return word.equals(((Word)obj).getWord());
    }

    //add a found word to the list
    public void addFind(int row_start, int column_start, int row_end, int column_end) {
        word_coordinates.add(new int[]{row_start, column_start, row_end, column_end});
    }
}
