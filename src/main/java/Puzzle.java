import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Puzzle {

    private char[][] board;
    private List<Word> wordsInOrder;
    private int rows;
    private int columns;

    //map of characters to list of words that begin with that character
    private Map<Character, List<Word>> words;

    //create from file
    public Puzzle (String filename) throws IOException {
        words = new HashMap<>();
        wordsInOrder = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            int line_index = 0;
            while ((line = br.readLine()) != null) {
                if (line_index == 0) {
                    //first line, create the board
                    board = createBoard(line);
                } else if (line_index <= board.length) {
                    //load each row
                    board[line_index - 1] = processRow(line.toUpperCase());
                } else {
                    //Load the words
                    processWord(line.toUpperCase());
                }

                line_index++;
            }
        }
    }

    //Load a single word
    private void processWord(String line) {
        //skip invalid words
        if  (line == null || line.trim().length() <= 0) {
            return;
        }

        char first = line.trim().charAt(0);

        //create list if doesnt exist
        if (!words.containsKey(first)) {
            words.put(first, new ArrayList<>());
        }

        //put word in list, avoid duplicates, remove spaces
        Word new_word = new Word(line.trim().replace(" ", ""));
        if (!words.get(first).contains(new_word)) {
            words.get(first).add(new_word);
            wordsInOrder.add(new_word);
        }
    }

    //get row from line
    private char[] processRow(String line) {
        char[] characters = line.replaceAll(" ", "").toCharArray();
        if (characters.length != columns) {
            throw new RuntimeException("number of columns incorrect for row: " + line);
        }

        return characters;
    }

    //create board from a string representation of board size ex:"3x3"
    private char[][] createBoard(String line) {
        try {
            String[] sizes = line.split("x");
            rows = Integer.valueOf(sizes[0]);
            columns = Integer.valueOf(sizes[1]);

            return new char[rows][];

        } catch (Exception e) {
            throw new RuntimeException("invalid board size string: " + line);
        }
    }

    //solve the puzzle, save coords in Word objects
    public void solve() {

        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                List<Word> possible = words.get(board[row][column]);

                if (possible != null && possible.size() > 0) {
                    findWordsAtCoord(row, column, possible);
                }
            }
        }


    }

    //starting at the given coordinate find all completed words and record them in their Word object
    private void findWordsAtCoord(int row, int column, List<Word> available_words) {
        //the 8 directions, as represented by deltas
        for (int row_delta = -1; row_delta < 2; row_delta++) {
            for (int col_delta = -1; col_delta < 2; col_delta++) {
                //dont try deltas 0,0
                if (row_delta != 0 || col_delta != 0) {
                    findWordsAtCoordInDirection(row, column, row_delta, col_delta, available_words);
                }
            }
        }
    }

    //starting at given coords and in given direction find all completed words and record them
    private void findWordsAtCoordInDirection(int row, int column, int row_delta, int col_delta, List<Word> available_words) {
        //avoid altering passed in list
        List<Word> possible = new ArrayList<>();
        possible.addAll(available_words);
        int char_index = 0;

        //walk through the words in the given direction until out of bounds or out of possible words
        while (inBounds(row + char_index * row_delta, column + char_index * col_delta) && possible.size() > 0) {
            List<Word> removals = new ArrayList<>();
            int current_row = row + char_index * row_delta;
            int current_col = column + char_index * col_delta;

            for (Word word : possible) {
                if (word.getWord().charAt(char_index) == board[current_row][current_col]) {
                    //check if word is complete, record it and remove word from possible
                    if (word.getWord().length() == char_index + 1) {
                        word.addFind(row, column, current_row, current_col);
                        removals.add(word);
                    }
                } else {
                    //word is no longer possible, mark for removal
                    removals.add(word);
                }
            }

            //remove all words no longer possible
            possible.removeAll(removals);

            //increment
            char_index++;
        }
    }

    //is given row,col within bounds of board?
    private boolean inBounds(int row, int column) {
        return row >= 0 && row < rows && column >= 0 && column < columns;
    }

    //print results to system.out
    public void printResults() {
        for (Word word : wordsInOrder) {
            for (int[] coords : word.getWord_coordinates()) {
                System.out.println(word.getWord() + " " + coords[0] +":" + coords[1] + " " + coords[2] +":" + coords[3]);
            }
        }
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public Map<Character, List<Word>> getWords() {
        return words;
    }
}
