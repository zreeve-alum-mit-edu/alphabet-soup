import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        //Change to filename of choice//
        Puzzle puzzle = new Puzzle("src\\test\\files\\large");
        puzzle.solve();
        puzzle.printResults();
    }
}
